'use strict';

$(document).ready(function () {
  $('.ui.dropdown').dropdown();

  $('.owl-slider').owlCarousel({
    dots: true,
    slideSpeed: 600,
    // autoHeight: true,
    paginationSpeed: 600,
    items: 1,
    itemsDesktop: false,
    itemsDesktopSmall: false,
    itemsTablet: false,
    itemsMobile: false,
    autoplay: true,
    autoplayTimeout: 1200,
    loop: true
  });

  $('.mobile-menu-control').click(function (event) {
    // console.log(123);
    $('.ui.sidebar').sidebar('toggle');
  });

  $('.mobile-submenu-control').click(function (event) {
    // console.log(123);
    $('.mobile-submenu').slideToggle('100');
  });

  $('.show-more .button').click(function () {
    $(this).hide();
    $('.rows--hidden').slideDown('400', function () {
      $('.close-up').show();
    });
  });

  $('.close-up').click(function () {
    $(this).hide();
    $('.rows--hidden').slideUp('400', function () {
      $('.show-more .button').show();
    });
  });

  $('.news-carousel').owlCarousel({
    nav: true,
    navContainer: '#news_nav',
    loop: true,
    margin: 50,
    itemsMobile: true,
    navText: ['<img src=\'images/nav_prev.png\'>', '<img src=\'images/nav_next.png\'>'],
    responsiveClass: true,
    responsive: {
      0: {
        items: 2
      },
      600: {
        items: 3
      },
      1000: {
        items: 4
      }
    }
  });

  $('.youtube-carousel').owlCarousel({
    nav: true,
    navContainer: '#youtube_nav',
    loop: true,
    margin: 50,
    itemsMobile: true,
    navText: ['<img src=\'images/nav_prev.png\'>', '<img src=\'images/nav_next.png\'>'],
    responsiveClass: true,
    responsive: {
      0: {
        items: 2
      },
      600: {
        items: 3
      },
      1000: {
        items: 4
      }
    }
  });

  $('.partners-carousel').owlCarousel({
    nav: true,
    navContainer: '#partners_nav',
    loop: true,
    margin: 50,
    itemsMobile: true,
    navText: ['<img src=\'images/nav_prev.png\'>', '<img src=\'images/nav_next.png\'>'],
    responsiveClass: true,
    responsive: {
      0: {
        items: 2
      },
      600: {
        items: 4
      },
      1000: {
        items: 7
      }
    }
  });

  $('.product-carousel').owlCarousel({
    nav: true,
    navContainer: '#partners_nav',
    loop: true,
    margin: 20,
    items: 5,
    itemsMobile: true,
    navText: ['<img src=\'images/nav_prev.png\'>', '<img src=\'images/nav_next.png\'>'],
    responsiveClass: true,
    responsive: {
      0: {
        items: 3
      },
      600: {
        items: 4
      },
      1000: {
        items: 5
      }
    }
  });

  $('.item-container').each(function () {
    var item = $('.item', $(this));
    var score = $('.score', $(this));
    var itemWidth = item.width();
    var scoreWidth = score.width();

    var offset1 = item.offset().left;
    var offset2 = score.offset().left;
    var fillerWidth = offset2 - offset1 - (itemWidth + scoreWidth);

    $('.fill', $(this)).css('width', fillerWidth + 10);
  });

  $('.control-r').on('click', function () {
    $('.product-carousel').trigger('next.owl.carousel');
    var cimage = $('.owl-carousel .owl-item.active img').attr('src');
    $('.product-image img').attr('src', cimage);
  });

  $('.control-l').on('click', function () {
    $('.product-carousel').trigger('prev.owl.carousel');
    var cimage = $('.owl-carousel .owl-item.active img').attr('src');
    $('.product-image img').attr('src', cimage);
  });

  $('.owl-item img').click(function() {
    console.log(12312);
    var image = $(this).attr('src');
    $('.product-image img').attr('src', image);
  });
  $('.catalog-details-header').click(function () {
    $('.catalog-details-content').slideToggle();
    $('.catalog-details-header .icon').toggleClass('down up');
  });

  // browser window scroll (in pixels) after which the "back to top" link is shown
  var offset = 300,


  //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
  offset_opacity = 1200,


  //duration of the top scrolling animation (in ms)
  scroll_top_duration = 700,


  //grab the "back to top" link
  $back_to_top = $('.cd-top');

  //hide or show the "back to top" link
  $(window).scroll(function () {
    $(this).scrollTop() > offset ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
    if ($(this).scrollTop() > offset_opacity) {
      $back_to_top.addClass('cd-fade-out');
    }
  });

  //smooth scroll to top
  $back_to_top.on('click', function (event) {
    event.preventDefault();
    $('body,html').animate({
      scrollTop: 0
    }, scroll_top_duration);
  });

  $('.close-icon').click(function () {
    $('.ui.modal').modal('hide');
  });

  $('.show-modal').click(function () {
    $('.ui.modal').modal('show');
  });

  var setMinHeight = function(minheight = 0) {
  jQuery('.owl-carousel').each(function(i,e){
    var oldminheight = minheight;
    jQuery(e).find('.owl-item').each(function(i,e){
      minheight = jQuery(e).height() > minheight ? jQuery(e).height() : minheight;
    });
    jQuery(e).find('.owl-item').css("min-height",minheight + "px");
    minheight = oldminheight;
    });
  };

  // setMinHeight(500);

});
